import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

//TODO Comments
//  Why I gave up threshold?

public class Main {

    public static BiMap<Object,Integer> dict = HashBiMap.create();
    public static Map<String,Object> cache = new HashMap<>();
    public static AtomicInteger counter = new AtomicInteger(0);

    public static  void main(String[] args){

        String folderPath = args[0];

        //TODO add validation on path
        File dir = new File(folderPath);
        File[] files = dir.listFiles();

        JSONParser jsonParser = new JSONParser();

        for (File file : files)
        {
            if (file.isFile() && file.getName().endsWith(".json")){
                Object myFile = null;
                try {
                    FileReader reader = new FileReader(file);
                    myFile = jsonParser.parse(reader);
                    if (myFile instanceof JSONArray){
                        handleJsonArray((JSONArray)myFile);
                    }
                    else{
                        handleJsonObject((JSONObject) myFile);
                        System.out.println("aaa");
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                cache.put(file.getName(),myFile);
            }

        }

        System.out.println("Hello World");

    }

    private static JSONObject handleJsonObject(JSONObject myFile) {
        Object[] keys = myFile.keySet().toArray();

        for (int i=0; i< keys.length; i++){
            Object jsonKey = keys[i];
            Integer compressedKey = getCompressedFromDict(jsonKey);
            Object jsonVal = myFile.get(jsonKey);

            if (jsonVal instanceof JSONObject){
                JSONObject compressedJson = handleJsonObject((JSONObject)jsonVal);
                myFile.put(compressedKey,compressedJson);
            } else{
                if (jsonVal instanceof JSONArray){
                    JSONArray compressedJson = handleJsonArray((JSONArray)jsonVal);
                    myFile.put(compressedKey,compressedJson);
                } else{ //json value is primitive (boolean,string or long)
                    Integer compressedVal = getCompressedFromDict(jsonVal);
                    myFile.put(compressedKey,compressedVal);
                }
            }
            myFile.remove(jsonKey);
        }

        return myFile;
    }

    private static Integer getCompressedFromDict(Object jsonKey) {
        Integer compressedKey = dict.get(jsonKey);
        if (compressedKey==null){
            compressedKey = getCounter();
            dict.put(jsonKey,compressedKey);
        }
        return compressedKey;
    }

    private static JSONArray handleJsonArray(JSONArray jsonArray) {

        for (int i=0; i<jsonArray.size(); i++){
            Object jsonElement = jsonArray.get(i);
            if (jsonElement instanceof  JSONArray){
                handleJsonArray((JSONArray)jsonElement);
            } else{
                if (jsonElement instanceof  JSONObject){
                    handleJsonObject((JSONObject)jsonElement);
                } else{ //primitive value in array
                    Integer compressedVal = getCompressedFromDict(jsonElement);
                    jsonArray.remove(i);
                    jsonArray.add(i,compressedVal);
                }
            }

        }
        return jsonArray;
    }

    private  static Integer getCounter(){
        int ans = counter.get();
        counter.compareAndSet(ans, ans+1);
        return ans;
    }
}
